import java.util.*;

public class PrimitiveCalculator {
    private static List<Integer> optimal_sequence(int n) {
        List<Integer> sequence = new ArrayList<Integer>();
        int[] lookup = new int[n+1];
        lookup[1] = 0;
        for (int i = 1; i <= n; i++) {
        	if (i + 1 <= n) {
	        	if (lookup[i + 1] == 0 || lookup[i + 1] > lookup[i] + 1)
	        		lookup[i + 1] = lookup[i] + 1;
        	}
        	if (i * 2 <= n) {
        		if (lookup[i * 2] == 0 || lookup[i * 2] > lookup[i] + 1)
        			lookup[i * 2] = lookup[i] + 1;       	
        	}
        	if (i * 3 <= n) {
        		if (lookup[i * 3] == 0 || lookup[i * 3] > lookup[i] + 1)
        			lookup[i * 3] = lookup[i] + 1;       	
        	}
        }
        for (int i = 1; i <= n; i++) 
        	//System.out.println("index: " + Integer.toString(i) + "  Steps: " + Integer.toString(lookup[i]));
        while (n >= 1) {
        	//System.out.print(n);
            sequence.add(n);
            if (lookup[n] == lookup[n - 1] + 1)
            	n = n - 1;
            else if (n % 2 == 0 && lookup[n / 2] == lookup[n] - 1)
            	n = n / 2;
            else
            	n = n / 3;
        }
        Collections.reverse(sequence);
        return sequence;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        List<Integer> sequence = optimal_sequence(n);
        System.out.println(sequence.size() - 1);
        for (Integer x : sequence) {
            System.out.print(x + " ");
        }
    }
}

