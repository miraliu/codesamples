import java.util.*;

public class DotProduct {
    private static long minDotProduct(int[] a, int[] b) {
        long result = 0;
        PriorityQueue<Integer> pq_a = new PriorityQueue<>((y,x) -> Integer.compare(x, y));
        PriorityQueue<Integer> pq_b = new PriorityQueue<>((x,y) -> Integer.compare(x, y));
        for (int i = 0; i < a.length; i++) {
        	pq_a.add(a[i]);
        	pq_b.add(b[i]);
        }
        while (!pq_a.isEmpty() && !pq_b.isEmpty())
        	result += (long) pq_a.poll() * pq_b.poll();
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        int[] b = new int[n];
        for (int i = 0; i < n; i++) {
            b[i] = scanner.nextInt();
        }
        System.out.println(minDotProduct(a, b));
    }
}

