import java.util.ArrayList;
import java.util.Scanner;

public class Fib {
  private static long calc_fib(int n) {
    ArrayList<Long> fiblist = new ArrayList<Long>();
    fiblist.add((long) 0);
    fiblist.add((long) 1);
    for (int i = 2; i <= n; i++) {
    	fiblist.add(fiblist.get(i-2) + fiblist.get(i-1));
    }
    return fiblist.get(n);
  }

  public static void main(String args[]) {
    Scanner in = new Scanner(System.in);
    int n = in.nextInt();

    System.out.println(calc_fib(n));
  }
}
