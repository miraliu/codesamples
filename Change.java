import java.util.Scanner;

public class Change {
    private static int getChange(int n) {
        int changes = n / 10;
        changes += (n % 10) / 5;
        changes += (n % 5);
        return changes;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(getChange(n));

    }
}

