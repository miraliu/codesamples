import java.util.*;

public class FibonacciHuge {
    private static long getFibonacciHuge(long n, long m) {
    	boolean period = false;
    	int i = 3;
    	ArrayList<Long> remainlist = new ArrayList<Long>();
    	remainlist.add((long) 0);
    	remainlist.add((long) 1);
    	remainlist.add((long) 1);
        while (!period) {
        	long j = (remainlist.get(i-2) + remainlist.get(i-1)) % m;
        	remainlist.add(j);
        	if (remainlist.get(i) == 1 && remainlist.get(i-1) == 0) {
        		period = true;
        	}
        	i = i + 1;
        }
        int periodlength = i - 2;
        long remain = n % periodlength;
        return remainlist.get((int) remain);
    }
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long n = scanner.nextLong();
        long m = scanner.nextLong();
        System.out.println(getFibonacciHuge(n, m));
    }
}

