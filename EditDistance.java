import java.util.*;

class EditDistance {
  public static int EditDistance(String s, String t) {
    //write your code here
	int[][] dist = new int[s.length() + 1][t.length() + 1];
	for (int p = 1; p < s.length() + 1; p++)
		dist[p][0] = p;
	for (int q = 1; q < t.length() + 1; q++)
		dist[0][q] = q;	
	for (int i = 0; i < s.length(); i++) {
		for (int j = 0; j < t.length(); j++) {
			int min_dist_prev = Integer.MAX_VALUE;
			if (min_dist_prev > dist[i][j + 1] + 1)
				min_dist_prev = dist[i][j + 1] + 1;		
			if (min_dist_prev > dist[i + 1][j] + 1)
				min_dist_prev = dist[i + 1][j] + 1;
			if (s.charAt(i) == t.charAt(j)) {
				dist[i + 1][j + 1] = Math.min(dist[i][j], min_dist_prev);
			} else {
				dist[i + 1][j + 1] = Math.min(dist[i][j] + 1, min_dist_prev);
			}
		}
	}
    return dist[s.length()][t.length()];
  }
  public static void main(String args[]) {
    Scanner scan = new Scanner(System.in);

    String s = scan.next();
    String t = scan.next();

    System.out.println(EditDistance(s, t));
  }

}
