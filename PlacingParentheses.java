import java.util.Scanner;

public class PlacingParentheses {
    private static long getMaximValue(String exp) {
    	String[] numberstr = exp.split("[^\\d]+");
    	long[] numbers = new long[numberstr.length];
    	for (int i = 0; i < numberstr.length; i++)
    		numbers[i] = Long.parseLong(numberstr[i]);
    	String[] opstr = exp.split("[\\s\\d]+");
    	char[] ops = new char[opstr.length - 1];
    	for (int i = 0; i < opstr.length - 1; i++) {
    		if (!opstr[i + 1].equals(""))
    			ops[i] = opstr[i + 1].charAt(0);
    	}
    	
    	

      return 0;
    }

    private static long eval(long a, long b, char op) {
        if (op == '+') {
            return a + b;
        } else if (op == '-') {
            return a - b;
        } else if (op == '*') {
            return a * b;
        } else {
            assert false;
            return 0;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String exp = scanner.next();
        System.out.println(getMaximValue(exp));
    }
}

