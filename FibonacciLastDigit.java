import java.util.*;

public class FibonacciLastDigit {
    private static int getFibonacciLastDigit(int n) {
        ArrayList<Integer> fiblist = new ArrayList<Integer>();
        fiblist.add(0);
        fiblist.add(1);
        for (int i = 2; i <= n; i++) {
        	fiblist.add((fiblist.get(i-2) + fiblist.get(i-1)) % 10);
        }
        return fiblist.get(n);
    }
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int c = getFibonacciLastDigit(n);
        System.out.println(c);
    }
}

