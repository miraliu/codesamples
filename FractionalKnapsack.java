import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Scanner;

public class FractionalKnapsack {
    private static double getOptimalValue(int capacity, int[] values, int[] weights) {
        double value = 0;
        Comparator<HashMap<String,Integer>> C = (y,x) -> Double.compare(((double) x.get("value") / x.get("weight")), ((double) y.get("value") / y.get("weight")));
        PriorityQueue<HashMap<String, Integer>> pq = new PriorityQueue(C);
        for (int i = 0; i < values.length; i++) {
        	HashMap<String, Integer> sort = new HashMap<>();
        	sort.put("value", values[i]);
        	sort.put("weight", weights[i]);
        	pq.add(sort);
        }
        while (capacity > 0 && !pq.isEmpty()) {
        	HashMap<String,Integer> item = pq.poll();
        	value += (double) item.get("value") / item.get("weight") * Integer.min(capacity, item.get("weight"));
        	capacity -= Integer.min(capacity, item.get("weight"));
        }

        return value;
    }

    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int capacity = scanner.nextInt();
        int[] values = new int[n];
        int[] weights = new int[n];
        for (int i = 0; i < n; i++) {
            values[i] = scanner.nextInt();
            weights[i] = scanner.nextInt();
        }
        System.out.println(getOptimalValue(capacity, values, weights));
    }
} 
