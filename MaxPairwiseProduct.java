import java.util.*;
import java.io.*;

public class MaxPairwiseProduct {
    static long getMaxPairwiseProduct(int[] numbers) {
        long result = 0;
        int n = numbers.length;
        int index1 = -1;
        int index2 = -1;
        int maxnumber = 0;
        for (int i = 0; i < n; i++) {
            if (index1 == -1 || numbers[i] >= maxnumber) {
            	index1 = i;
            	maxnumber = numbers[i];
            }
        }
        maxnumber = 0;
        for (int i = 0; i < n; i++) {
            if ((index2 == -1 || numbers[i] >= maxnumber) && index1 != i) {
            	index2 = i;
            	maxnumber = numbers[i];
            }
        }
//        System.out.println(index1);
//        System.out.println(index2);
        if (index1 != -1 && index2 != -1)
        	result = (long) numbers[index1] * numbers[index2];
        else if (index1 != -1)
        	result = (long) numbers[index1];
        else
        	result = 0;
        return result;
    }

    public static void main(String[] args) {
        FastScanner scanner = new FastScanner(System.in);
        int n = scanner.nextInt();
        int[] numbers = new int[n];
        for (int i = 0; i < n; i++) {
            numbers[i] = scanner.nextInt();
        }
        System.out.println(getMaxPairwiseProduct(numbers));
    }

    static class FastScanner {
        BufferedReader br;
        StringTokenizer st;

        FastScanner(InputStream stream) {
            try {
                br = new BufferedReader(new InputStreamReader(stream));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        String next() {
            while (st == null || !st.hasMoreTokens()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }

        int nextInt() {
            return Integer.parseInt(next());
        }
    }

}