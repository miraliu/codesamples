import MapReduce
import sys

"""
Word Count Example in the Simple Python MapReduce Framework
"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    # key: document identifier
    # value: document contents
    mr.emit_intermediate(record[0], [record[1],'out'])
    mr.emit_intermediate(record[1], [record[0],'in'])

def reducer(key, list_of_values):
    # key: word
    # value: list of occurrence counts
    total = {}
    for v in list_of_values:
        if total.has_key(v[0]) == False:
            total[v[0]] = [v[1]]
        else:
            total[v[0]].append(v[1])
    for rkey,rvalue in total.items():
        if len(rvalue) == 1:
            mr.emit((rkey, key))

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
