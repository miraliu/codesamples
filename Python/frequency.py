import sys
import json
import re

def main():
    #sent_file = open(sys.argv[1])
    tweet_file = open(sys.argv[1])
    #tweet_file = open("problem_1_submission.txt")
    tweet(tweet_file)

def tweet(file):
    tf = {}
    totalterm = 0
    for line in file:
        record = json.loads(line,encoding="utf-8")
        if record.has_key("text") == False:
            continue
        if record.has_key("lang") and record["lang"] != "en":
            continue
        p = re.compile('\w+')
        content = p.findall(record["text"])
        for word in content:
            totalterm += 1
            if tf.has_key(word):
                tf[word] += 1.0
            if tf.has_key(word) == False:
                tf[word] = 1.0
    for key,value in tf.items():
        print key + " " + str(value/totalterm)



if __name__ == '__main__':
    main()
