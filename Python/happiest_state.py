import sys
import json
import re

def hw():
    print 'Hello, world!'

def lines(fp):
    print str(len(fp.readlines()))

def main():
    sent_file = open(sys.argv[1])
    tweet_file = open(sys.argv[2])
    #sent_file = open("AFINN-111.txt")
    #tweet_file = open("output.txt")
    sc = sent(sent_file)
    tweet(tweet_file, sc)

def sent(file):
    scores = {} # initialize an empty dictionary
    for line in file:
        term, score  = line.split("\t")  # The file is tab-delimited. "\t" means "tab character"
        scores[term] = int(score)  # Convert the score to an integer.
    #print scores.items() # Print every (term, score) pair in the dictionary
    return scores

def tweet(file, score):
    states = {
        'AK': 'Alaska',
        'AL': 'Alabama',
        'AR': 'Arkansas',
        'AS': 'American Samoa',
        'AZ': 'Arizona',
        'CA': 'California',
        'CO': 'Colorado',
        'CT': 'Connecticut',
        'DC': 'District of Columbia',
        'DE': 'Delaware',
        'FL': 'Florida',
        'GA': 'Georgia',
        'GU': 'Guam',
        'HI': 'Hawaii',
        'IA': 'Iowa',
        'ID': 'Idaho',
        'IL': 'Illinois',
        'IN': 'Indiana',
        'KS': 'Kansas',
        'KY': 'Kentucky',
        'LA': 'Louisiana',
        'MA': 'Massachusetts',
        'MD': 'Maryland',
        'ME': 'Maine',
        'MI': 'Michigan',
        'MN': 'Minnesota',
        'MO': 'Missouri',
        'MP': 'Northern Mariana Islands',
        'MS': 'Mississippi',
        'MT': 'Montana',
        'NA': 'National',
        'NC': 'North Carolina',
        'ND': 'North Dakota',
        'NE': 'Nebraska',
        'NH': 'New Hampshire',
        'NJ': 'New Jersey',
        'NM': 'New Mexico',
        'NV': 'Nevada',
        'NY': 'New York',
        'OH': 'Ohio',
        'OK': 'Oklahoma',
        'OR': 'Oregon',
        'PA': 'Pennsylvania',
        'PR': 'Puerto Rico',
        'RI': 'Rhode Island',
        'SC': 'South Carolina',
        'SD': 'South Dakota',
        'TN': 'Tennessee',
        'TX': 'Texas',
        'UT': 'Utah',
        'VA': 'Virginia',
        'VI': 'Virgin Islands',
        'VT': 'Vermont',
        'WA': 'Washington',
        'WI': 'Wisconsin',
        'WV': 'West Virginia',
        'WY': 'Wyoming'
        }
    for key in states.keys():
        states[key] = 0

    for line in file:
        record = json.loads(line,encoding="utf-8")
        if record.has_key("text") == False:
            continue
        if record.has_key("lang") and record["lang"] != "en":
            continue
        p = re.compile('\w+')
        content = p.findall(record["text"])
        sent = 0
        for word in content:
            if score.has_key("word"):
                sent += score["word"]
        if record.has_key("place") and record['place'] != None:
            pl = record["place"]
            if pl.has_key("full_name"):
                fn = pl['full_name']
                fn_parts = p.findall(fn)
                if len(fn_parts) == 0:
                    continue
                if states.has_key(fn_parts[-1]):
                    states[fn_parts[-1]] += 1
    max = 0
    maxst = ""
    for key,value in states.items():
        if value >= max:
            maxst = key
            max = value
    print maxst



if __name__ == '__main__':
    main()
