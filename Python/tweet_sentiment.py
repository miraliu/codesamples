import sys
import json

def hw():
    print 'Hello, world!'

def lines(fp):
    print str(len(fp.readlines()))

def main():
    sent_file = open(sys.argv[1])
    tweet_file = open(sys.argv[2])
    sc = sent(sent_file)
    tweet(tweet_file, sc)

def sent(file):
    scores = {} # initialize an empty dictionary
    for line in file:
        term, score  = line.split("\t")  # The file is tab-delimited. "\t" means "tab character"
        scores[term] = int(score)  # Convert the score to an integer.
    #print scores.items() # Print every (term, score) pair in the dictionary
    return scores

def tweet(file, score):
    for line in file:
        record = json.loads(line,encoding="utf-8")
        if record.has_key("text") == False:
            continue
        content = record["text"].split(" ")
        sent = 0
        for word in content:
            if score.has_key("word"):
                sent += score["word"]
        print sent



if __name__ == '__main__':
    main()
