import sys
import json

def hw():
    print 'Hello, world!'

def lines(fp):
    print str(len(fp.readlines()))

def main():
    #sent_file = open(sys.argv[1])
    #tweet_file = open(sys.argv[2])
    sent_file = open("AFINN-111.txt")
    tweet_file = open("output.txt")
    sc = sent(sent_file)
    tweet(tweet_file, sc)

def sent(file):
    scores = {} # initialize an empty dictionary
    for line in file:
        term, score  = line.split("\t")  # The file is tab-delimited. "\t" means "tab character"
        scores[term] = int(score)  # Convert the score to an integer.
    #print scores.items() # Print every (term, score) pair in the dictionary
    return scores

def tweet(file, score):
    termsent = {}
    for line in file:
        record = json.loads(line,encoding="utf-8")
        if record.has_key("text") == False:
            continue
        content = record["text"].split(" ")
        sent = 0
        for word in content:
            if score.has_key(word):
                sent += score[word]
        for word in content:
            if termsent.has_key(word):
                if sent > 0:
                    termsent[word][0] += 1.0
                if sent < 0:
                    termsent[word][1] += 1.0
            if termsent.has_key(word) == False:
                if sent > 0:
                    termsent[word] = [1.0,0.0]
                elif sent < 0:
                    termsent[word] = [0.0,1.0]
                else:
                    termsent[word] = [0.0,0.0]
    for key,value in termsent.items():
        print key + " " + str((value[0]+1)/(value[1]+1))



if __name__ == '__main__':
    main()
