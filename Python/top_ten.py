import sys
import json
import operator

def main():
    #sent_file = open(sys.argv[1])
    #tweet_file = open(sys.argv[1])
    tweet_file = open("output.txt")
    tweet(tweet_file)

def tweet(file):
    tf = {}
    totalterm = 0
    for line in file:
        record = json.loads(line,encoding="utf-8")
        if record.has_key("entities") == False:
            continue
        elif record["entities"]['hashtags'] == []:
            continue
        content_ = record["entities"]['hashtags'][0]
        if content_.has_key('text'):
            content = content_['text']
            if tf.has_key(content):
                tf[content] += 1
            else:
                tf[content] = 1
        else:
            continue
    sorted_x = sorted(tf.items(), key=operator.itemgetter(1), reverse=True)

    for i in range(0,min(10,len(sorted_x))):
        #print str(sorted_x)
        print (sorted_x[i][0] + " " + str(sorted_x[i][1]))


if __name__ == '__main__':
    main()
