import java.util.*;
import java.io.*;

public class MajorityElement {
    private static int getMajorityElement(int[] a, int left, int right) {
        if (left == right) {
            return -1;
        }
        if (left + 1 == right) {
            return a[left];
        }
        //write your code here
        int mid = Math.floorDiv((right + left), 2);
        //System.out.println("Mid: " + Integer.toString(mid));
        int left_majority = getMajorityElement(a, left, mid);
        int right_majority = getMajorityElement(a, mid, right);
        if (left_majority == right_majority) {
        	//System.out.println("return agreed majority: " + Integer.toString(left_majority));
        	return left_majority;
        } else {
        	int left_count = 0;
        	int right_count = 0;
        	for (int i = left; i < right; i++) {
       		//System.out.print(Integer.toString(a[i])+", ");
        		if (a[i] == left_majority)
        			left_count += 1;
        		else if (a[i] == right_majority)
        			right_count += 1;
        		if (left_count >= Math.floorDiv((right - left), 2) + 1 ) {
//        			System.out.println("");
//        			System.out.println("return left majority: " + Integer.toString(left_majority));
        			return left_majority;
        		}
        		else if (right_count >=Math.floorDiv((right - left), 2) + 1 ) {
        			return right_majority;
        		}
        	}
        }
        return -1;
    }

    public static void main(String[] args) {
        FastScanner scanner = new FastScanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        if (getMajorityElement(a, 0, a.length) != -1) {
            System.out.println(1);
        } else {
            System.out.println(0);
        }
    }
    static class FastScanner {
        BufferedReader br;
        StringTokenizer st;

        FastScanner(InputStream stream) {
            try {
                br = new BufferedReader(new InputStreamReader(stream));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        String next() {
            while (st == null || !st.hasMoreTokens()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }

        int nextInt() {
            return Integer.parseInt(next());
        }
    }
}

